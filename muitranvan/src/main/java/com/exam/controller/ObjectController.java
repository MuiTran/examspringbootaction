package com.exam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.exam.form.UserForm;
import com.exam.service.UserServiceInterface;

@RestController
@RequestMapping("/")
public class ObjectController {
	@Autowired
	UserServiceInterface userService;

	/**
	 * get User name
	 * @param id
	 * @return
	 */
	@GetMapping("/restServiceGetUser")
	public UserForm restServicetUserName(@RequestParam("id") Long id) {
		return userService.getUsernameByID(id);
	}

	/**
	 * Get Username, StartDate, Membership based on id
	 * @param id
	 * @return
	 */
	@GetMapping("/restServiceMemberShipStartDate")
	public UserForm restServiceMemberShipStartDate(@RequestParam("id") Long id) {
		return userService.getMemberShipStartDateByID(id);
	}
	
	/**
	 * Get ID, Username, StartDate, Loyal_point based on id
	 * @param id
	 * @return
	 */
	@GetMapping("/restServiceGetUserInfo")
	public UserForm restServiceGetUserInfo(@RequestParam("id") Long id) {
		return userService.getUserByID(id);
	}

}

