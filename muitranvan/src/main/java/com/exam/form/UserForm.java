package com.exam.form;

import java.util.Date;

/**
 * 
 * @author muitranv1
 *
 */
public class UserForm {
	
	/**
	 * to show only user name
	 * @param name
	 */
	public UserForm(String name) {
		this.name = name;
	}

	/**
	 * to show Name, Startdate, membership
	 * @param name
	 * @param startDate
	 * @param membership
	 */
	public UserForm(String name, Date startDate, String membership) {
		this.name = name;
		this.startDate = startDate;
		this.membership = membership;
	}
	
	/**
	 * to show id, name, startdate, loyalpoint
	 * @param id
	 * @param name
	 * @param startDate
	 * @param loyalpoint
	 */
	public UserForm(Long id, String name, Date startDate, Long loyalpoint) {
		this.id = id;
		this.name = name;
		this.startDate = startDate;
		this.loyalPoint = loyalpoint;
	}
	
	private Long id;

	private String name;

	private Date startDate;

	private String membership;
	
	private Long loyalPoint;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public String getMembership() {
		return membership;
	}

	public void setMembership(String membership) {
		this.membership = membership;
	}

	public Long getLoyalPoint() {
		return loyalPoint;
	}

	public void setLoyalPoint(Long loyalPoint) {
		this.loyalPoint = loyalPoint;
	}
	

}
