package com.exam.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.exam.entity.Account;

/**
 * 
 * @author muitranv1
 *
 */
@Repository
public interface AccountDAO extends JpaRepository<Account, String>{

}
