package com.exam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author muitranv1
 *
 */
@SpringBootApplication
public class MuitranvanApplication {

	public static void main(String[] args) {
		SpringApplication.run(MuitranvanApplication.class, args);
	}
}
