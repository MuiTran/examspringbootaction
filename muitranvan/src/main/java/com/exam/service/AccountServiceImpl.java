package com.exam.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.exam.dao.AccountDAO;
import com.exam.entity.Account;

@Service
public class AccountServiceImpl implements AccountServiceInterface {
	@Autowired
	AccountDAO accountDAO;
	
	/**
	 * get Account via accountDAO
	 */
	@Override
	public Account getAccountByUsername(String username) throws Exception {
		return accountDAO
				.findById(username)
				.orElseThrow(() -> new Exception("Can't find User with Username: " + username));
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		try {
			return getAccountByUsername(username);
		} catch (Exception e) {
			throw new UsernameNotFoundException("UsernameNotFoundException", e);
		}
	}

}
