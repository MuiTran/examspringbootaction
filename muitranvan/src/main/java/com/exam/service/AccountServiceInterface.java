package com.exam.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.exam.entity.Account;

public interface AccountServiceInterface extends UserDetailsService {

	/**
	 * get Account to login based on user name
	 * @param username
	 * @return
	 * @throws Exception
	 */
	public Account getAccountByUsername(String username) throws Exception;
}
