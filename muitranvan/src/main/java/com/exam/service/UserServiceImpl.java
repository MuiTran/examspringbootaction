package com.exam.service;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exam.dao.UserDAO;
import com.exam.entity.User;
import com.exam.form.UserForm;

/**
 * 
 * @author muitranv1
 *
 */
@Service
public class UserServiceImpl implements UserServiceInterface {
	@Autowired
	UserDAO userDAO;
	
	/**
	 * GET User Name based on id
	 */
	@Override
	public UserForm getUsernameByID(Long id) {
		Optional<User> result = userDAO.findById(id);
		return new UserForm(result.get().getName());
	}

	/**
	 * Get Username, StartDate, Membership based on id
	 */
	@Override
	public UserForm getMemberShipStartDateByID(Long id) {
		Optional<User> result = userDAO.findById(id);
		User user = result.get();
		return new UserForm(user.getName(), user.getStartDate(), user.getMembership());
	}

	/**
	 * Get ID, Username, StartDate, Loyal_point based on id
	 */
	@Override
	public UserForm getUserByID(Long id) {
		User user = userDAO.findById(id).get();
		return new UserForm(user.getId(), user.getName(), user.getStartDate(),getLoyalPoint(user.getStartDate()) );
	}
	
	/**
	 * Calculate the Loyal_Point with value as ( today - startDate + 1 )*5
	 * @param startDate
	 * @return
	 */
	private long getLoyalPoint(Date startDate) { 
		
		Calendar c1 = Calendar.getInstance();
		Calendar c2 = Calendar.getInstance();
		
		Date today = new Date();

		c1.setTime(today);
		c2.setTime(startDate);
		
	    return ((Math.abs(c1.get(Calendar.DAY_OF_MONTH)-c2.get(Calendar.DAY_OF_MONTH)))+1)*5;  
	} 

}
