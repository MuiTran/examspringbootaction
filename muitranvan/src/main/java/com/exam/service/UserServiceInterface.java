package com.exam.service;

import com.exam.form.UserForm;

/**
 * 
 * @author muitranv1
 *
 */
public interface UserServiceInterface {
	
	/**
	 * GET User Name based on id
	 * @param id
	 * @return
	 */
	public UserForm getUsernameByID(Long id);

	/**
	 * Get Username, StartDate, Membership based on id
	 * @param id
	 * @return
	 */
	public UserForm getMemberShipStartDateByID(Long id);

	/**
	 * Get ID, Username, StartDate, Loyal_point based on id
	 * @param id
	 * @return
	 */
	public UserForm getUserByID(Long id);
}
