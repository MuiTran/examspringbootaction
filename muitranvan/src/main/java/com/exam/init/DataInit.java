package com.exam.init;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.exam.dao.AccountDAO;
import com.exam.dao.UserDAO;
import com.exam.entity.Account;
import com.exam.entity.User;

@Component
public class DataInit implements ApplicationRunner {
	@Autowired
	private UserDAO userDAO;

	@Autowired
	private AccountDAO accountDAO;

	@Autowired
	private BCryptPasswordEncoder encrypt;

	private static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

	/**
	 * define initially value for Users and Accounts
	 */
	@Override
	public void run(ApplicationArguments args) throws Exception {
		long count = userDAO.count();
		long count1 = accountDAO.count();
		if (count == 0 && count1 == 0) {
			User p1 = new User();
			User p2 = new User();
			Date d1 = df.parse("2017-1-20");
			Date d2 = df.parse("2018-1-20");

			p1.setName("Mui1");
			p1.setStartDate(d1);
			p1.setMembership("cui bap");

			p2.setName("Mui2");
			p2.setStartDate(d2);
			p2.setMembership("cui bap");

			userDAO.save(p1);
			userDAO.save(p2);

			String[] userRoles1 = {"USER"};
			Account account1 = new Account();
			account1.setUsername("user");
			account1.setPassword(encrypt.encode("123456"));
			account1.setUserRoles(userRoles1);
			account1.setEnabled(true);
			account1.setAccountNonExpired(true);
			account1.setAccountNonLocked(true);
			account1.setCredentialsNonExpired(true);
			
			String[] userRoles2 = {"ADMIN"};
			Account account2 = new Account();
			account2.setUsername("admin");
			account2.setPassword(encrypt.encode("123456"));
			account2.setUserRoles(userRoles2);
			account2.setEnabled(true);
			account2.setAccountNonExpired(true);
			account2.setAccountNonLocked(true);
			account2.setCredentialsNonExpired(true);

			accountDAO.save(account1);
			accountDAO.save(account2);

		}

	}

}