package com.exam.config;

import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import com.exam.entity.Account;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * 
 * @author muitranv1
 *
 */
@SuppressWarnings("unchecked")
public class TokenAuthenticationService {
	private TokenAuthenticationService() {}
	public static void addAuthentication(HttpServletResponse res, Authentication authResult) {
		Claims claims = Jwts.claims().setSubject(authResult.getName());
		claims.put("roles", authResult.getAuthorities());
		String jwt = Jwts.builder().setClaims(claims)
				.setExpiration(new Date(System.currentTimeMillis() + 10000))
				.signWith(SignatureAlgorithm.HS512, "mysercret").compact();
		res.addHeader("header_string", "BEARER" + " " + jwt);
	}

	public static Authentication getAuthentication(HttpServletRequest request) {
		String token = request.getHeader("header_string");
		String secretKey = request.getHeader("mysercret");
		if (token != null && secretKey != null) {
			// parse the token.
			Claims claim = Jwts.parser().setSigningKey(secretKey)
					.parseClaimsJws(token.replace("BEARER", "")).getBody();

			ArrayList<String> listRoles = (ArrayList<String>) claim.get("roles");
			String[] rolesArray = {};

			Account account = new Account();
			account.setUsername(claim.getSubject());
			account.setUserRoles(rolesArray);

			return claim != null
					? new UsernamePasswordAuthenticationToken(account.getUsername(), null, account.getAuthorities())
					: null;
		}
		return null;
	}


}
